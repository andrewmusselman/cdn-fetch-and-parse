#!/usr/bin/env python3
import sys

already_seen_date_user_asset = {}
block_list = ["#Version", "#Software", "#Fields", "fps.ezdrm.com", "subtitles", "favicon", "/audio/", "/https"]

for line in sys.stdin:
    if any(x in line for x in block_list):
        continue
    a = line.split("\t")
    b = a[4].split("/")
    if len(b) < 8:
        continue
    sys.stdout.write(a[0] + "\t" + a[1] + "\t" +  a[2] + "\t" + b[3] + "\t" + b[4] + "\t" + b[7] + "\t" + a[7] + "\t" + a[9] + "\n")
