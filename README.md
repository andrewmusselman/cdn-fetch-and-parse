# README #

This is a summary of how to fetch CDN logs, parse them, and load the data into the Analytics database.

### Environment ###

* Host where this work is deployed: superset.24imedia.tv
* Crontab which runs these scripts: `0 5 * * * /home/ec2-user/cdn-fetch-and-parse/daily-batch.sh`
* Script which re-runs parsing for all time: `parallelize-parsing.sh`
* Script which runs parsing for one day: `parse-fields.py`

### Running Scripts ###

* Daily batch: run daily by cron job at 05:00 UTC for the previous day
* Parallel parsing: `parallelize-parsing.sh` (note the output file is hard-coded in here)
* Parse one day: `parse-fields.sh <year> <month> <day> > /data/parsed-logs/daily-<year>-<month>-<day>.tsv`
* Manually load parsed data into database (should be done automatically in `daily-batch.sh`): `psql --user postgres -d superset -c "\copy views from '/data/parsed-logs/daily-<year>-<month>-<day>.tsv' delimiter E'\t';"`

### Database Schema ###

* Schema for `views` table:
```
superset=# \d views
              Table "public.views"
   Column   |          Type          | Modifiers 
------------+------------------------+-----------
 date       | date                   | 
 time       | time without time zone | 
 ip         | inet                   | 
 checksum   | character varying(60)  | 
 media      | character varying(100) | 
 segment    | character varying(60)  | 
 time_taken | character varying(10)  | 
 ua         | character varying(400) | 
```

### Who do I talk to? ###

* Andrew Musselman andrew.musselman@24i.com
* [PRDDATA Confluence Page](https://24imedia.atlassian.net/wiki/spaces/PRDDATA/overview)
