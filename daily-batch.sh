#!/bin/bash
Y=`date -d "12 hours ago" '+%Y'`
M=`date -d "12 hours ago" '+%m'`
D=`date -d "12 hours ago" '+%d'`
mkdir -p /data/caching/$Y/$M/$D
rsync -va broadwaylogsna@broadway-logs-na.ingest.cdn.level3.net:/logs/fd_booxmedia/$Y/$M/$D/ /data/caching/$Y/$M/$D
#$HOME/parse-fields.sh $Y $M $D > /data/parsed-logs/daily-$Y-$M-$D.tsv
#psql --user postgres -d superset -c "\copy views from '/data/parsed-logs/daily-$Y-$M-$D.tsv' delimiter E'\t';"
